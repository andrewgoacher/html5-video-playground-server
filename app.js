import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import streamRouter from './routes/stream';
import cors from 'cors';

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/stream', streamRouter);

const port = 8086;
let server = app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});


export default app;
