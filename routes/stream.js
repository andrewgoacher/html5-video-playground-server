import express from 'express';
const router = express.Router();

import uuid from 'uuid/v1';
import path from 'path';
import fs from 'fs';

router.get('/:vid/:part/', (req, res, next) => {
    const vid = req.params.vid;
    const part = req.params.part;

    let file = `${vid}_${part}.webm`;

    const file_path = path.join(__dirname, '../data/vid/outputs', file);

    fs.readFile(file_path, (err, data) => {
        if(err) {
            res.status(500).send("Problem finding chunk");
            return;
        }

        // res.setHeader('content-type', 'application/octet-stream');
        // res.setHeader('content-disposition', `attachment; filename=${file}`);
        res.send(data);
    });
});


export default router;